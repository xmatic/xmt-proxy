package xyz.xmatic.proxy;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import xyz.xmatic.proxy.security.JwtConfig;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProxyApplicationTests {

	@Autowired
	JwtConfig jwtConfig;

	@Test
	public void contextLoads() {
	}

	@Test
	public void test1() {
		System.out.println(jwtConfig.getPrefix());
	}

}
